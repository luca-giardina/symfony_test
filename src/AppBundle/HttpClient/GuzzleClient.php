<?php

namespace AppBundle\HttpClient;

use GuzzleHttp\Client;

class GuzzleClient implements HttpClientInterface {
	private $HttpClient;

	public function __construct() {
		$this->HttpClient = new Client();
	}

	public function get($endpoint, $params = null) {
		$request = $this->HttpClient->get($endpoint, $params);
		return json_decode($request->getBody(), true);
	}

	public function sayHi() { return 'GuzzleClient'; }
}