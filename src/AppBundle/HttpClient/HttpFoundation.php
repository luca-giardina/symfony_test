<?php

namespace AppBundle\HttpClient;

use Symfony\Component\HttpFoundation\Request;

class HttpFoundation implements HttpClientInterface {
	private $HttpClient;

	public function __construct() {
		$this->HttpClient = new Request();
	}

	public function get($endpoint, $params) {
		// $this->HttpClient->get($endpoint, $params);
	}

	public function sayHi() { return 'HttpFoundation'; }
}