<?php

namespace AppBundle\HttpClient;

class HttpClient {

	private static $instance;

    public static function getInstance($http_client)
    {
        if (null === static::$instance) {
        	switch ($http_client) {
        		case 'guzzle':
		            static::$instance = new GuzzleClient();
        			break;
				case 'foundation':
		            static::$instance = new HttpFoundation();
        			break;
        		
        		default:
        			static::$instance = new GuzzleClient();
        			break;
        	}
        }
        return static::$instance;
    }

    public static function changeHttpClient($http_client) {

        self::$instance = null;
        return self::getInstance($http_client);
    }

    protected function __construct() {}
    protected function __clone() {}
    protected function __wakeup() {}
}