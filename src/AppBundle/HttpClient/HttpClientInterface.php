<?php

namespace AppBundle\HttpClient;

interface HttpClientInterface {
	public function get($endpoint, $params);
}