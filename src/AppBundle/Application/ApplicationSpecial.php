<?php

namespace AppBundle\Application;

class ApplicationSpecial implements ApplicationInterface {


    public function id($aOffer) {
        return $aOffer["app_details"]["bundle_id"];
    }

    public function payout($aOffer) {
        if( !isset($aOffer["campaigns"]) || !isset($aOffer["campaigns"]["points"]) )
            return "0.00";

        // TODO
        // parent method to convert amount
        return number_format( $aOffer["campaigns"]["points"] / 1000, 2);
    }

    public function country($aOffer) { 
        // TODO
        // parent method to convert TO ISO ecc.
        if( isset($aOffer["campaigns"]["countries"][0]) )
            return substr($aOffer["campaigns"]["countries"][0], 0, 2);
        return null;
    }

    public function name($aOffer) { return $aOffer["app_details"]["developer"] ?? null; }
    public function platform($aOffer) { return $aOffer["app_details"]["platform"] ?? null; }
}