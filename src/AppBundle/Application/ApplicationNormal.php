<?php

namespace AppBundle\Application;

class ApplicationNormal implements ApplicationInterface {

    public function id($aOffer) {
        return $aOffer["mobile_app_id"];
    }

    public function payout($aOffer) {
        if( $aOffer["payout_currency"] == "USD")
            return number_format($aOffer["payout_amount"], 2);

        // TODO
        // parent method to convert amount
        return number_format($aOffer["payout_amount"], 2);
    }

    public function country($aOffer) {
        // TODO
        // parent method to convert TO ISO ecc.
        if( isset($aOffer["countries"][0]) ) 
            return substr($aOffer["countries"][0], 0, 2);
        return null; 
    }

    public function name($aOffer) { return $aOffer["name"] ?? null; }
    public function platform($aOffer) { return $aOffer["mobile_platform"] ?? null; }
}