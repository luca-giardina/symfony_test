<?php

namespace AppBundle\Application;

interface ApplicationInterface {
	public function payout($aOffer);
	public function id($aOffer);
	public function country($aOffer);
	public function name($aOffer);
	public function platform($aOffer);
}