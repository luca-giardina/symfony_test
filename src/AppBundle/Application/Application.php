<?php

namespace AppBundle\Application;

class Application {
	private $Application;

    public function __construct($Application) {
    	$this->Application = $Application;
    }

    public function getApplicationId($aOffer) { return $this->Application->id($aOffer); }
    public function getApplicationPayout($aOffer) { return $this->Application->payout($aOffer); }
    public function getApplicationCountry($aOffer) { return $this->Application->country($aOffer); }
    public function getApplicationName($aOffer) { return $this->Application->name($aOffer); }
    public function getApplicationPlatform($aOffer) { return $this->Application->platform($aOffer); }
}