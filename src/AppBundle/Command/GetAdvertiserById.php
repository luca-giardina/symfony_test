<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Logger\ConsoleLogger;
use AppBundle\HttpClient\HttpClient;
use AppBundle\Application\ApplicationNormal;
use AppBundle\Application\ApplicationSpecial;
use AppBundle\Application\Application;
use AppBundle\Entity\Offer;

class GetAdvertiserById extends ContainerAwareCommand
{
	const END_POINT = "http://process.xflirt.com/advertiser/";

	protected function configure()
	{
	    $this
	        // the name of the command (the part after "bin/console")
	        ->setName('getadvertiser:byid')

	        // the short description shown while running "php bin/console list"
	        ->setDescription('Get the advertisers by advertiser_id')

	        // the full command description shown when running the command with
	        // the "--help" option
	        ->setHelp('Get the advertisers by advertiser_id, parameters: advertiser_id')
	        // adding argument REQUIRED
	        ->addArgument('advertiser_id', InputArgument::REQUIRED, 'the advertiser id.')
	        // adding argument OPTIONAL
	        ->addArgument('http_client', InputArgument::OPTIONAL, 'the http client to use for the request.')
	    ;
	}

    /**
     * @return AdvertiserInterface
     */
	private function getApplicationType($aOffer) {
		// maybe the check is wrong but the time is passing by..

		if( isset($aOffer["mobile_app_id"]) )
			return new ApplicationNormal;

		if( isset($aOffer["app_details"]) && isset($aOffer["app_details"]["bundle_id"]) )
			return new ApplicationSpecial;

		return false;
	}

	private function storeOffers($aOffers) {

		foreach ($aOffers as $k => $aOffer) {

			$oApplicationType = $this->getApplicationType($aOffer);

			if( $oApplicationType ) {
				$oApplication = new Application($oApplicationType);
			}
			else {
				return [ "result" => false, "log" => "important log infos" ];
			}

			$sAppId = $oApplication->getApplicationId($aOffer);
			
			$oEm = $this->getContainer()->get('doctrine')->getManager();;
			$oRepository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Offer');

			// TODO
			// on insert I should add this ID to cache so I will avoid a query (check if already exist)
			// hoping Doctrine is smart enough!

			$oOffer = $oRepository->findOneByApplicationId( $sAppId );

			if (!$oOffer) {
	    		$oOffer = new Offer();
	    		$oOffer->setApplicationId($sAppId);
			}

			$oOffer->setCountry($oApplication->getApplicationCountry($aOffer));
			$oOffer->setPayout($oApplication->getApplicationPayout($aOffer));
			$oOffer->setName($oApplication->getApplicationName($aOffer));
			$oOffer->setPlatform($oApplication->getApplicationPlatform($aOffer));

			// prepare
		    $oEm->persist($oOffer);

		    // actually executes the queries (i.e. the INSERT query)
		    $oEm->flush();
		}
		
		return [ "result" => true ]; 
	}

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$logger = new ConsoleLogger($output);

    	$input_advertiser_id = $input->getArgument('advertiser_id');

    	if( !is_numeric($input_advertiser_id) ) {
    		$logger->info('agument must be a number, value: ' . $input_advertiser_id);
    	}
    	else
    	{
    		$input_http_client = $input->getArgument('http_client') ?? 'guzzle';

			$HttpClient = HttpClient::getInstance($input_http_client);
			try {
				$aOffers = $HttpClient->get(GetAdvertiserById::END_POINT . $input_advertiser_id . '/offers');

				$logger->info($this->storeOffers($aOffers));
			}
			catch (\Exception $e) {
				$logger->info('impossible fetch data, advertiser_id: ' . $input_advertiser_id);
			}
			
		}
    }
}