GETTING STARTED
========================



 * Create user and config database and be ready to insert data on symfony 3 installation

 * git clone https://luca-giardina@bitbucket.org/luca-giardina/symfony_test.git symfony_demo

 * cd symfony_demo && composer install

 * php bin/console doctrine:database:create

  * php bin/console doctrine:schema:update --force


**expected:**

Updating database schema...

Database schema updated successfully! "1" query was executed



  * php bin/console doctrine:schema:validate

**expected:**

[Mapping]  OK - The mapping files are correct.

[Database] OK - The database schema is in sync with the mapping files.





Using the Command
--------------

**php bin/console getadvertiser:byid :advertiser_id :http_client**


  * :advertiser_id must be a number

  * :http_client default is guzzle (guzzle is the working one..)


**N.B.**
Errors and results loggin by monolog (not tested)






Missing
--------------

**ENTITY Offer**

  * Entity field validation: i tried to use symfony validation but I need to read more about it because I'm pretty sure is not working like this.



**Application Model**

  * method to convert amount in USD when is not USD
  * method to convert country to ISO 3166-1 alpha-2

**COMMENTS INLINE IN THE CODE**




Design document
--------------

Would be nice to create a UML diagrams but, long story short, I'm using the command as a Controller and the main pattern is MVC

- HttpClient class is a Singleton using a Strategy pattern depends on the user input

- HttpClient fetch the data from the constant GetAdvertiserById::END_POINT adding the advertiser_id

- the method storeOffers foreach offer of that advertiser define the strategy thanks to getApplicationType getting the object oApplicationType that is a instance of ApplicationInterface

- Allocate the Application class (strategy pattern) and ask the Application object all what we need to store the Offer entity








Symfony Standard Edition
========================

**WARNING**: This distribution does not support Symfony 4. See the
[Installing & Setting up the Symfony Framework][15] page to find a replacement
that fits you best.

Welcome to the Symfony Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

What's inside?
--------------

The Symfony Standard Edition is configured with the following defaults:

  * An AppBundle you can use to start coding;

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

It comes pre-configured with the following bundles:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev env) - Adds code generation
    capabilities

  * [**WebServerBundle**][14] (in dev env) - Adds commands for running applications
    using the PHP built-in web server

  * **DebugBundle** (in dev/test env) - Adds Debug and VarDumper component
    integration

All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!

[1]:  https://symfony.com/doc/3.4/setup.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/3.4/doctrine.html
[8]:  https://symfony.com/doc/3.4/templating.html
[9]:  https://symfony.com/doc/3.4/security.html
[10]: https://symfony.com/doc/3.4/email.html
[11]: https://symfony.com/doc/3.4/logging.html
[13]: https://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html
[14]: https://symfony.com/doc/current/setup/built_in_web_server.html
[15]: https://symfony.com/doc/current/setup.html
